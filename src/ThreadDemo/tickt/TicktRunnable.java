package ThreadDemo.tickt;

public class TicktRunnable implements Runnable{

    private int ticket = 5;

    @Override
    public void run() {

        for (int i = 0; i < 100; i++) {
            if (ticket > 0) {
                System.out.println(Thread.currentThread().getName() + "正在出售第" + (ticket--) + "张票");
            }
        }
    }

        public static void main (String[]args){
            TicktRunnable ticktRunnable = new TicktRunnable();
            Thread t1 = new Thread(ticktRunnable);
            Thread t2 = new Thread(ticktRunnable);
            Thread t3 = new Thread(ticktRunnable);
            Thread t4 = new Thread(ticktRunnable);

            t1.start();
            t2.start();
            t3.start();
            t4.start();
        }
    }
