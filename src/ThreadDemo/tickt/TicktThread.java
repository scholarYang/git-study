package ThreadDemo.tickt;

public class TicktThread extends Thread{

    private int tickt = 5;

    @Override
    public void run() {
        for (int i  = 0;i<100;i++){
            if (tickt>0){
                System.out.println(Thread.currentThread().getName()+"正在出售第"+(tickt--)+"张票");
            }
        }
    }

    public static void main(String[] args) {
        TicktThread t1 = new TicktThread();
        TicktThread t2 = new TicktThread();
        TicktThread t3 = new TicktThread();
        TicktThread t4 = new TicktThread();


        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}
