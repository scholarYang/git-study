package ThreadDemo;


/*
* 实现多线程的时候：
*   1、需要继承Thread类
*   2、必须重写run()方法，值得是核心执行的逻辑
*   3、线程在启动的时候，不要直接调用run()，而是要通过start()来进行调用
*   4、每次运行相同的代码，出来的结果可能不一样。原因在于，多线程谁先抢占资源，无法进行人为控制
* 第二种实现方式：  使用了代理设计模式
*   1、实现Runnable()接口
*   2、重写run()方法
*   3、创建Thread对象，将刚刚创建好的runnable的子类作为thread的构造参数
*   4、通过thread.start()进行启动
*
*   两种实现方式哪种用的比较多
*   推荐使用第二种。
*       1、java是单继承，将继承关系留给最需要的类
*       2、使用Runnable接口之后不需要给共享变量添加static关键字，每次创建一个对象作为共享对象即可。
* */
public class ThreadDemo extends Thread{
    @Override
    public void run() {
        for (int i=1;i<=10;i++){
            System.out.println(Thread.currentThread().getName()+"---------"+i);
        }
    }

    public static void main(String[] args) {
        ThreadDemo threadDemo01 = new ThreadDemo();
        threadDemo01.start();

        for (int i=1;i<=5;i++){
            System.out.println(Thread.currentThread().getName()+"==========="+i);
        }
    }
}
