package ThreadDemo.proxy;

public class Wangpo implements KindWomen{

    private KindWomen kindWomen = new PanJInLian();

    public Wangpo(){

    }

    public Wangpo(KindWomen kindWomen){
        this.kindWomen = kindWomen;
    }
    @Override
    public void makeEyesWithMen() {
        this.kindWomen.makeEyesWithMen();
    }

    @Override
    public void playWithMen() {
        this.kindWomen.playWithMen();
    }
}
