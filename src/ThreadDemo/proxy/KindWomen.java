package ThreadDemo.proxy;

public interface KindWomen {

    /**
     * make eyes with men--抛媚眼
     */
    public void makeEyesWithMen();

    public void playWithMen();
}
